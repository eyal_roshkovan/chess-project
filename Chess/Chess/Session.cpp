#include "Session.h"

#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

#include <iostream>
#include <string>
#include <tuple>

void Session::end_game()
{
    _tableUtility.~TableUtility();
}

Session::Session(const std::string& newTable): _currPlayer(0)
{
    _tableUtility = TableUtility();
}



// All frontend requests about changing pieces position including attacking arrive here
int Session::updateTable(const std::string& command)
{
  int oldX, newX;
  int oldY, newY;

  oldX = (int)command[commandIndexes::oldX] - (int)'a';
  oldY = (int)command[commandIndexes::oldY] - (int)'0' - 1;
  newX = (int)command[commandIndexes::newX] - (int)'a';
  newY = (int)command[commandIndexes::newY] - (int)'0' - 1;
  

  if (_tableUtility.get_color_of_square(std::make_tuple(oldY, oldX)) != _currPlayer)
      return 2;

  if (_tableUtility.get_color_of_square(std::make_tuple(newY, newX)) == _currPlayer)
      return 3;

  int code = (_tableUtility).move(std::make_tuple(oldY, oldX), std::make_tuple(newY, newX)); // table row and col indexes start from 1
  
  if (code == 0 || code == 1)
    _currPlayer = (1+_currPlayer)%2; // The next player will be chosen when the first one makes an allowed move
  //}

  return code;
}

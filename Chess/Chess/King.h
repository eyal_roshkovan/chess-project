#pragma once
#include "Piece.h"

class King : public Piece
{
public:
	King(bool _type, tuple <int, int> _coords);
	virtual int isPossibleMove(const tuple<int, int> &new_coords) const override;

};

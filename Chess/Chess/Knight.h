#pragma once
#include "Piece.h"
#include <iostream>
#include <vector>

class Knight : public Piece
{
public:
	Knight(bool _type, tuple <int, int> _coords);
	virtual int isPossibleMove(const tuple<int, int>& new_coords) const override;
	virtual int is_check(const tuple<int, int>& king_location) const override;
private:
	std::vector<tuple<int, int >> _possibleMoves;
	void addPossibleMoves();
};

#include "Bishop.h"
Bishop::Bishop(bool _type, tuple <int, int> _coords):Piece(_type, _coords, bishop) { }

int Bishop::isPossibleMove(const tuple<int, int>& new_coords) const
{
	if (Piece::isPossibleMove(new_coords) == 0)
	{
		return 0;
	}
	tuple<int, int> current = get_coords();
	int old_x = std::get<1>(current);
	int old_y = std::get<0>(current);
	int new_x = std::get<1>(new_coords);
	int new_y = std::get<0>(new_coords);

	for (int i = 0; i + old_x < 8 && i + old_y < 8; i++)
	{
		if (i + old_x == new_x && i + old_y == new_y)
		{
			return 1;
		}
	}
	for (int i = 0; i + old_x < 8 && old_y - i >= 0; i++)
	{
		if (i + old_x == new_x && old_y - i == new_y)
		{
			return 2;
		}
	}
	for (int i = 0; old_x - i >= 0 && old_y - i >= 0; i++)
	{
		if (old_x - i == new_x && old_y - i == new_y)
		{
			return 3;
		}
	}
	for (int i = 0; old_x - i >= 0 && old_y + i < 8; i++)
	{
		if (old_x - i == new_x && old_y + i == new_y)
		{
			return 4;
		}
	}
	return 0;
}

int Bishop::is_check(const tuple<int, int>& king_location) const
{
	if(isPossibleMove(king_location) > 0)
  {
		return isPossibleMove(king_location);
  }

	return 0;
}

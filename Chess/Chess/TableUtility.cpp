#include "TableUtility.h"

using std::make_tuple;
#include <list>

TableUtility::TableUtility()
{
	generate_pawns();
	generate_bishops();
	generate_kings();
	generate_knight();
	generate_nulls();
	generate_queens();
	generate_rooks();
}

void TableUtility::generate_pawns()
{
	for (int i = 0; i < 8; i++)
	{
		table[1][i] = new Pawn(true, std::make_tuple(1, i));
		table[6][i] = new Pawn(false, std::make_tuple(6, i));
	}
}

void TableUtility::generate_bishops()
{
	table[0][2] = new Bishop(true, std::make_tuple(0, 2));
	table[0][5] = new Bishop(true, std::make_tuple(0, 5));
	table[7][2] = new Bishop(false, std::make_tuple(7, 2));
	table[7][5] = new Bishop(false, std::make_tuple(7, 5));
}

void TableUtility::generate_knight()
{
	table[0][1] = new Knight(true, make_tuple(0, 1));
	table[0][6] = new Knight(true, make_tuple(0, 6));
	table[7][1] = new Knight(false, make_tuple(7, 1));
	table[7][6] = new Knight(false, make_tuple(7, 6));
}

void TableUtility::generate_rooks()
{
	table[0][0] = new Rook(true, make_tuple(0, 0));
	table[0][7] = new Rook(true, make_tuple(0, 7));
	table[7][0] = new Rook(false, make_tuple(7, 0));
	table[7][7] = new Rook(false, make_tuple(7, 7));

}

void TableUtility::generate_queens()
{
	table[0][4] = new Queen(true, make_tuple(0, 4));
	table[7][4] = new Queen(false, make_tuple(7, 4));
}

void TableUtility::generate_kings()
{
	table[0][3] = new King(true, make_tuple(0, 3));
	table[7][3] = new King(false, make_tuple(7, 3));
}

void TableUtility::generate_nulls()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 2; j < 6; j++)
		{
			table[j][i] = nullptr;
		}
	}
}

Piece* TableUtility::GetPoint(tuple<int, int> coords)
{
	return (table)[get<0>(coords)][get<1>(coords)];
}

bool TableUtility::is_threatening_to_the_point(Piece* piece, tuple<int, int> point)
{
	if (!piece->is_check(point))
		return false;

	// {pawn = 1, knight = 2, king = 3, rook = 4, queen = 5, bishop = 6}

	if (piece->get_type() == 4)
	{
		if (piece->isPossibleMove(point) == 1) // Rook is going up
		{
			for (int i = 1; get<0>(piece->get_coords()) + i < get<0>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()) + i, get<1>(piece->get_coords()))) != nullptr) // If the square in the middle of the way is alreay taken
					return false;
			}
		}
		else if (piece->isPossibleMove(point) == 2) // Rook is going right
		{
			for (int i = 1; get<1>(piece->get_coords()) + i < get<1>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()), get<1>(piece->get_coords()) + i)) != nullptr) // If the square in the middle of the way is alreay taken
					return false;
			}
		}
		else if (piece->isPossibleMove(point) == 3) // Rook is going down
		{
			for (int i = 1; get<0>(piece->get_coords()) - i > get<0>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()) - i, get<1>(piece->get_coords()))) != nullptr) // If the square in the middle of the way is alreay taken
					return false;
			}
		}
		else // Rook is going left
		{
			for (int i = 1; get<1>(piece->get_coords()) - i > get<1>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()), get<1>(piece->get_coords()) - i)) != nullptr) // If the square in the middle of the way is alreay taken
					return false;
			}
		}
	}
	else if (piece->get_type() == 5)
	{
		Rook a(piece->get_color(), piece->get_coords());
		if (is_threatening_to_the_point(&a, point))
			return is_threatening_to_the_point(&a, point);

		Bishop b(piece->get_color(), piece->get_coords());
		return is_threatening_to_the_point(&b, point);
	}
	else if (piece->get_type() == 6)
	{
		if (piece->isPossibleMove(point) == 1) // Bishop is going right up
		{
			for (int i = 1; get<0>(piece->get_coords()) + i < get<0>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()) + i, get<1>(piece->get_coords()))) != nullptr) // If the square is not empty and the bishop can't move through it
					return false;
			}
		}
		else if (piece->isPossibleMove(point) == 2) // Bishop is going right down
		{
			for (int i = 1; get<0>(piece->get_coords()) - i > get<0>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()) - i, get<1>(piece->get_coords()) + i)) != nullptr) // If the square is not empty and the bishop can't move through it
					return false;

			}
		}
		else if (piece->isPossibleMove(point) == 3) // Bishop is  going left down
		{
			for (int i = 1; get<0>(piece->get_coords()) - i > get<0>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()) - i, get<1>(piece->get_coords()) - i)) != nullptr) // If the square is not empty and the bishop can't move through it
					return 0;

			}
		}
		else // Bishop is going left up
		{
			for (int i = 1; get<1>(piece->get_coords()) - i > get<1>(point); i++)
			{
				if (GetPoint(make_tuple(get<0>(piece->get_coords()) + i, get<1>(piece->get_coords()) - i)) != nullptr) // If the square is not empty and the bishop can't move through it
					return 0;
			}
		}
	}

	return true;
}


bool TableUtility::check(tuple<int, int> src, tuple<int, int> dst) // recieves 2 white kings
{
	Piece* piece_to_move = GetPoint(src); // white king
	(table)[get<0>(dst)][get<1>(dst)] = piece_to_move;
	(table)[get<0>(src)][get<1>(src)] = nullptr;
	bool color = piece_to_move->get_color();// color is white
	if (piece_to_move->get_type() == 3)
	{
		if (canKingGoThere(color, piece_to_move->get_coords()))
		{
			(table)[get<0>(src)][get<1>(src)] = GetPoint(dst);
			(table)[get<0>(dst)][get<1>(dst)] = piece_to_move;
			return true;
		}
		(table)[get<0>(src)][get<1>(src)] = GetPoint(dst);
		(table)[get<0>(dst)][get<1>(dst)] = piece_to_move;
	}
	else
	{
		if (canKingGoThere(color, GetKing(piece_to_move->get_color())))
		{
			(table)[get<0>(src)][get<1>(src)] = GetPoint(dst);
			(table)[get<0>(dst)][get<1>(dst)] = nullptr;
			return true;
		}
		(table)[get<0>(src)][get<1>(src)] = GetPoint(dst);
		(table)[get<0>(dst)][get<1>(dst)] = nullptr;
	}

	return false;
}

/*
The fucntion checks if the move with pawn is legal
Input: src square and dst square
Output: 0 - unable, 1 - simple move, 2 - eating
*/
int TableUtility::isPossibleMovePawn(tuple<int, int> src, tuple<int, int> dst)
{
	int x = get<1>(src);
	int y = get<0>(src);

	if (GetPoint(src)->get_color()) // If the selected piece is white
	{
		if (GetPoint(src)->isPossibleMove(dst) == 3) // If move means eating another piece to the right (basic rules)
		{
			if ((GetPoint(dst)) == nullptr || GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // If the dst square is empty or the piece in it has the same color as the src color - Then impossible to eat
				return 3;

			if (check(src, dst))
				return 0;

			return 4;
		}
		else if (GetPoint(src)->isPossibleMove(dst) == 4) // If move means eating another piece to the left (basic rules)
		{
			if (GetPoint(dst) == nullptr || GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // If the dst square is empty or the piece in it has the same color as the src color - Then impossible to eat
				return 3;

			if (check(src, dst))
				return 0;

			return 4;
		}

		if (GetPoint(make_tuple(y + 1, x)) != nullptr) // If the square up to the src square is not empty - Then unable to get there
			return 6;

		if (GetPoint(src)->isPossibleMove(dst) == 2) // If we want to go 2 steps instead of 1
		{
			if (GetPoint(dst) != nullptr) // If the square is not empty - Then unable to get there
				return 3;

			if (check(src, dst))
				return 0;

			return 4;
		}
	}
	else // If the selected piece is black
	{
		if (GetPoint(src)->isPossibleMove(dst) == 3) // If move means eating another piece to the right (basic rules)
		{
			if (GetPoint(dst) == nullptr || GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // If the dst square is empty or the piece in it has the same color as the src color - Then impossible to eat
				return 3;

			if (check(src, dst))
				return 0;

			return 4;
		}
		else if (GetPoint(src)->isPossibleMove(dst) == 4) // If move means eating another piece to the left (basic rules)
		{
			if (GetPoint(dst) == nullptr || GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // If the dst square is empty or the piece in it has the same color as the src color - Then impossible to eat
				return 3;

			if (check(src, dst))
				return 0;

			return 4;
		}

		if (GetPoint(make_tuple(y - 1, x)) != nullptr) // If the square up to the src square is not empty - Then unable to get there
			return 6;

		if (GetPoint(src)->isPossibleMove(dst) == 2) // If we want to go 2 steps instead of 1
		{
			if (GetPoint(dst) != nullptr) // If the square is not empty - Then unable to get there
				return 3;

			if (check(src, dst))
				return 0;

			return 4;
		}
	}
	if (check(src, dst))
		return 0;
	return 4;
}

int TableUtility::isPossibleMoveQueen(tuple<int, int> src, tuple<int, int> dst)
{
	int rookPossible = isPossibleMoveRook(src, dst);
	if (rookPossible == 0) // The queen has same skills as bishop and rook, so we are going to check each of them, now we are checking the rook
		return rookPossible;

	return isPossibleMoveBishop(src, dst); // Checking the bishop, if the move is illegal then we will get the 0 from the bishop
}

int TableUtility::isPossibleMoveRook(tuple<int, int> src, tuple<int, int> dst)
{
	if (GetPoint(src)->isPossibleMove(dst) == 1) // Rook is going up
	{
		for (int i = 1; get<0>(src) + i < get<0>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src) + i, get<1>(src))) != nullptr) // If the square in the middle of the way is alreay taken
				return 4;
		}
	}
	else if (GetPoint(src)->isPossibleMove(dst) == 2) // Rook is going right
	{
		for (int i = 1; get<1>(src) + i < get<1>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src), get<1>(src) + i)) != nullptr) // If the square in the middle of the way is alreay taken
				return 4;
		}
	}
	else if (GetPoint(src)->isPossibleMove(dst) == 3) // Rook is going down
	{
		for (int i = 1; get<0>(src) - i > get<0>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src) - i, get<1>(src))) != nullptr) // If the square in the middle of the way is alreay taken
				return 4;
		}
	}
	else // Rook is going left
	{
		for (int i = 1; get<1>(src) - i > get<1>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src), get<1>(src) - i)) != nullptr) // If the square in the middle of the way is alreay taken
				return 4;
		}
	}

	if (GetPoint(dst) != nullptr) // If the destination square is not empty
	{
		if (GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // If the destination square is alerady taken by other piece with the same color
			return 3;

		if (check(src, dst))
			return 2;

	}

	if (check(src, dst))
		return 0;

	return 1;

}

/*
The fucntion checks if the move with bishop is legal
Input: src square and dst square
Output: 0 - unable, 1 - simple move, 2 - eating
*/

int TableUtility::isPossibleMoveBishop(tuple<int, int> src, tuple<int, int> dst)
{
	if (GetPoint(src)->isPossibleMove(dst) == 1) // Bishop is going right up
	{
		for (int i = 1; get<0>(src) + i < get<0>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src) + i, get<1>(src) + i)) != nullptr) // If the square is not empty and the bishop can't move through it
				return 4;
		}
	}
	else if (GetPoint(src)->isPossibleMove(dst) == 2) // Bishop is going right down
	{
		for (int i = 1; get<0>(src) - i > get<0>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src) - i, get<1>(src) + i)) != nullptr) // If the square is not empty and the bishop can't move through it
				return 4;

		}
	}
	else if (GetPoint(src)->isPossibleMove(dst) == 3) // Bishop is  going left down
	{
		for (int i = 1; get<0>(src) - i > get<0>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src) - i, get<1>(src) - i)) != nullptr) // If the square is not empty and the bishop can't move through it
				return 4;

		}
	}
	else // Bishop is going left up
	{
		for (int i = 1; get<1>(src) - i > get<1>(dst); i++)
		{
			if (GetPoint(make_tuple(get<0>(src) + i, get<1>(src) - i)) != nullptr) // If the square is not empty and the bishop can't move through it
				return 4;
		}
	}
	if (GetPoint(dst) != nullptr) // If the destination square is not empty
	{
		if (GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // If the destination square is alerady taken by other piece with the same color
			return 3;

	}
	if (check(src, dst))
		return 0;

	return 4;
}

int TableUtility::isPossibleMoveKnight(tuple<int, int> src, tuple<int, int> dst)
{
	if (GetPoint(src)->isPossibleMove(dst) == 0) // If move is not possible (basic rules)
		return 6;

	if (GetPoint(dst) != nullptr && GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // checking if the dst square is already taken by another piece with the same color
		return 3;

	/*
	if (GetPoint(dst) != nullptr && GetPoint(dst)->get_color() != GetPoint(src)->get_color()) // checking if the dst square is not already taken by another piece with the same color
	{
		if (check(src, dst))
			return 2;
	}
	*/
	if (check(src, dst))
		return 0;

	return 4;
}


int TableUtility::isPossibleMoveKing(tuple<int, int> src, tuple<int, int> dst)
{
	if (GetPoint(src)->isPossibleMove(dst) == 0) // If move is not possible (basic rules)
		return 6;

	if (GetPoint(dst) != nullptr && GetPoint(dst)->get_color() == GetPoint(src)->get_color()) // checking if the dst square is already taken by another piece with the same color
		return 3;

	if (check(src, dst))
		return 0;

	return 4;
}

int TableUtility::check_amount_of_pieces(bool color) const
{
	int counter = 0;
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (table[i][j] != nullptr && (table)[j][i]->get_color() == color)
				counter++;
		}
	}
	return counter;
}

tuple<int, int> TableUtility::GetKing(bool color)
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (GetPoint(make_tuple(i, j)) != nullptr && GetPoint(make_tuple(i, j))->get_type() == 3 && GetPoint(make_tuple(i, j))->get_color() == color)
				return make_tuple(i, j);
		}
	}

	return make_tuple(0, 0);
}

bool TableUtility::canKingGoThere(bool color, tuple<int, int> dst) // recieves white and king dst
{
	std::vector<Piece*> enemies = getAllPieces(!color); // enemies must contain only black pieces
	for (int i = 0; i < enemies.size(); i++)
	{
		if (is_threatening_to_the_point(enemies[i], dst))
			return false;
	}
	return true;
}

std::vector<Piece*> TableUtility::getAllPieces(bool color)
{
	std::vector<Piece*> pieces;
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if ((table)[i][j] != nullptr && (table)[i][j]->get_color() == color)
				pieces.push_back((table[i][j]));
		}
	}
	return pieces;
}

bool TableUtility::check_mate(bool color)
{
	std::vector<Piece*> my_pieces = getAllPieces(color);
	std::vector<Piece*> enemies = getAllPieces(!color);
	bool found = false;
	for (int piece = 0; piece < my_pieces.size(); piece++)
	{
		for (int i = 0; i < tableSize; i++)
		{
			for (int j = 0; j < tableSize; j++)
			{
				if (my_pieces[piece]->get_type() == 3)
				{
					Piece* prev = GetPoint(make_tuple(i, j));
					if (isPossibleMove(my_pieces[piece]->get_coords(), make_tuple(i, j)) == 0)
					{
						for (int enemy = 0; enemy < enemies.size(); enemy++)
						{
							if (is_threatening_to_the_point(enemies[enemy], make_tuple(i, j)))
							{
								found = true;
							}
						}
						table[i][j] = prev;
						if (!found)
							return false;
					}

				}
				else
				{
					if (i == 1 && j == 7 && piece == 7)
						std::cout << "abc;";
					if (isPossibleMove(my_pieces[piece]->get_coords(), make_tuple(i, j)) == 0)
						return false;
				}
			}
		}
	}
	return true;


}

int TableUtility::isPossibleMove(tuple<int, int> src, tuple<int, int> dst)// pawn, knight, king, rook, queen, bishop
{
	if (GetPoint(src) == nullptr)
		return 2;

	if (src == dst)
		return 7;

	if (GetPoint(src)->isPossibleMove(dst) == 0) // If move is not possible (basic rules)
		return 6;

	if (GetPoint(src)->get_type() == 1)
	{
		return (isPossibleMovePawn(src, dst));
	}
	else if (GetPoint(src)->get_type() == 2)
	{
		return isPossibleMoveKnight(src, dst);
	}
	else if (GetPoint(src)->get_type() == 3)
	{
		return isPossibleMoveKing(src, dst);
	}
	else if (GetPoint(src)->get_type() == 4)
	{
		return isPossibleMoveRook(src, dst);
	}
	else if (GetPoint(src)->get_type() == 5)
	{
		return isPossibleMoveQueen(src, dst);
	}
	else if (GetPoint(src)->get_type() == 6)
	{
		return isPossibleMoveBishop(src, dst);
	}
	return 1;
}

int TableUtility::move(tuple<int, int> src, tuple<int, int> dst)
{
	if (src == dst)
		return 7;

	if (GetPoint(src)->isPossibleMove(dst) == 0) // If move is not possible (basic rules)
		return 6;

	if (isPossibleMove(src, dst) == 0)
	{
		bool color = true;
		if (get_color_of_square(src) == 0)
			color = false;
		(table)[get<0>(src)][get<1>(src)]->move(dst);
		(table)[get<0>(dst)][get<1>(dst)] = GetPoint(src);
		(table)[get<0>(src)][get<1>(src)] = nullptr;
		if (!check(GetKing(!color), GetKing(!color)))
		{
			if (check_mate(!color))
			{
				return 8;
			}
			return 1;
		}
		return 0;
	}
	return isPossibleMove(src, dst);

}

int TableUtility::get_color_of_square(tuple<int, int> point)
{
	if (GetPoint(point) == nullptr)
		return 3;

	if (GetPoint(point)->get_color())
		return 1;

	return 0;

}

#include "King.h"

King::King(bool _type, tuple<int, int> _coords) : Piece(_type, _coords, king) { }

int King::isPossibleMove(const tuple<int, int>& new_coords) const
{
	if (Piece::isPossibleMove(new_coords) == 0)
		return 0;

	int new_x = std::get<1>(new_coords);
	int new_y = std::get<0>(new_coords);
	int old_x = std::get<1>(get_coords());
	int old_y = std::get<0>(get_coords());

	if (new_x - old_x < 2 && new_x - old_x > -2 && new_y - old_y < 2 && new_y - old_y > -2)
		return 1;


	return 0;
}

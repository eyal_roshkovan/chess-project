#include "Piece.h"

int Piece::isPossibleMove(const tuple<int, int>& new_coords) const
{
	if (std::get<0>(new_coords) > 7 || std::get<0>(new_coords) < 0 || std::get<1>(new_coords) > 7 || std::get<1>(new_coords) < 0) // checking if in the table
		return 0;
	
	if (std::get<0>(_coords) == std::get<0>(new_coords) && std::get<1>(_coords) == std::get<1>(new_coords)) // checking if the same point
		return 0;
	
	return 1;
}

Piece::Piece(bool type, const tuple<int, int>& coords, actualPiece actual_piece)
	: _type{ type }, _actual_piece{ actual_piece }, _is_active{ true }, _coords(coords)
{ }

actualPiece Piece::get_type() const
{
	return _actual_piece;
}

bool Piece::get_is_active() const
{
	return _is_active;
}

void Piece::disactive()
{
	_is_active = false;
}

int Piece::is_check(const tuple<int, int>& king_location) const
{
	return 0;
}

bool Piece::get_color() const
{
	return _type;
}

tuple<int, int> Piece::get_coords() const
{
	return _coords;
}

void Piece::move(tuple<int, int> new_coords)
{
	_coords = new_coords;
}


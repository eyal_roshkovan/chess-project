#pragma once
#include "Piece.h"

class Pawn : public Piece
{
public:
	Pawn(bool _type, tuple <int, int> _coords);
	virtual int isPossibleMove(const tuple<int, int>& new_coords) const override;
	virtual int is_check(const tuple<int, int>& king_location) const override;
};

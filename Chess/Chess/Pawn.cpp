#include "Pawn.h"

Pawn::Pawn(bool _type, tuple<int, int> _coords):Piece(_type, _coords, pawn)
{}

int Pawn::isPossibleMove(const tuple<int, int>& new_coords) const
{
	if (Piece::isPossibleMove(new_coords) == 0)
	{
		return 0;
	}
	tuple<int, int> current = get_coords();
	int old_x = std::get<1>(current);
	int old_y = std::get<0>(current);
	int new_x = std::get<1>(new_coords);
	int new_y = std::get<0>(new_coords);
	if (get_color())
	{
		if (new_y <= old_y)
			return 0;
	}
	else
	{
		if (old_y <= new_y)
			return 0;
	}
	if (new_x != old_x)
	{
		if (new_y - 1 == old_y || new_y + 1 == old_y)
		{
			if (new_x - 1 == old_x)
				return 3;
			
			if (new_x + 1 == old_x)
				return 4;

		}
		return 0;
	}
	if (new_y - old_y == 2 && get_color() && old_y == 1)
	{
		return 2;
	}
	if (new_y - old_y == -2 && !get_color() && old_y == 6)
	{
		return 2;
	}
	if (new_y - old_y == 1)
	{
		return 1;
	}
	if (new_y - old_y == -1)
	{
		return 1;
	}
	
	return 0;
}

int Pawn::is_check(const tuple<int, int>& king_location) const
{
	if (isPossibleMove(king_location) > 2)
  {
		return isPossibleMove(king_location) + 4;
  }

	return 0;
}

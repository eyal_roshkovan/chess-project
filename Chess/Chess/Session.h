#pragma once

#include "TableUtility.h"

#include "Piece.h"

#include <string>

#define tableSize 8

#define whitePieces false
#define blackPieces true

typedef Piece* chessBoard[tableSize][tableSize];

class Session
{
public:
	enum commandIndexes { oldX, oldY, newX, newY};
	void end_game();

	Session(const std::string& newTable);
	~Session() = default;

	int updateTable(const std::string& command);

private:
	TableUtility _tableUtility;
	//Piece* _table[tableSize][tableSize];
	//chessBoard _table;
	static const int firstPlayerIndicator = 63;
	int _currPlayer; // Bool is the only data type that accepts only 2 values thus saving runtime memory, there's nothing to do against black chess pieces
};


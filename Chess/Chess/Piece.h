#pragma once
#include <iostream>
#include <tuple>
#include <string>
enum actualPiece{pawn = 1, knight = 2, king = 3, rook = 4, queen = 5, bishop = 6};
using std::tuple;

class Piece
{
public:

	Piece(bool type, const tuple <int, int>& _coords, actualPiece _actual_piece);
	~Piece() = default;

	bool get_color() const;
	tuple<int, int> get_coords() const;
	void move(tuple <int, int> new_coords);
	actualPiece get_type() const;
	bool get_is_active() const;
	void disactive();
	virtual int isPossibleMove(const tuple<int, int> &new_coords) const;
	virtual int is_check(const tuple<int, int>& king_location) const;

private:
	tuple <int, int> _coords;
	bool _type;
	actualPiece _actual_piece;
	bool _is_active;
};

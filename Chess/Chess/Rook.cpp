#include "Rook.h"

Rook::Rook(bool _type, tuple<int, int> _coords) : Piece(_type, _coords, rook) {}


int Rook::isPossibleMove(const tuple<int, int>& new_coords) const //up is 1, right is 2, down is 3, left is 4
{
	if (Piece::isPossibleMove(new_coords) == 0)
	{
		return 0;
	}
	tuple<int, int> current = get_coords();
	int old_x = std::get<1>(current);
	int old_y = std::get<0>(current);
	int new_x = std::get<1>(new_coords);
	int new_y = std::get<0>(new_coords);
	if (old_x == new_x)
	{
		if (old_y < new_y)
		{
			return 1;
		}
		return 3;
	}
	if (old_y == new_y)
	{
		if (old_x < new_x)
		{
			return 2;
		}
		return 4;
	}
	return 0;

}

int Rook::is_check(const tuple<int, int>& king_location) const
{
	if (isPossibleMove(king_location) > 0)
	{
		return isPossibleMove(king_location) + 4;
	}
	return 0;
}

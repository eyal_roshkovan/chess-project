#include "Queen.h"

Queen::Queen(bool _type, tuple<int, int> _coords) : Piece(_type, _coords,  queen)
{
}

int Queen::isPossibleMove(const tuple<int, int>& new_coords) const
{
	if (Piece::isPossibleMove(new_coords) == 0)
	{
		return 0;
	}
	Rook a(get_color(), get_coords());
	if (a.isPossibleMove(new_coords) != 0)
	{
		return a.isPossibleMove(new_coords);
	}
	Bishop b(get_color(), get_coords());
	return b.isPossibleMove(new_coords);

}

int Queen::is_check(const tuple<int, int>& king_location) const
{
	Rook a(get_color(), get_coords());
	if (a.is_check(king_location) != 0)
		return a.is_check(king_location);

	Bishop b(get_color(), get_coords());
	if (b.is_check(king_location) != 0)
		return b.is_check(king_location);

	return 0;
}

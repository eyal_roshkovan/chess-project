#include "Bishop.h"
#include "Piece.h"
#include "King.h"
#include "Knight.h"
#include "Queen.h"
#include "Rook.h"
#include "Pawn.h"

#define tableSize 8

typedef Piece* chessBoard[tableSize][tableSize];
using std::get;

class TableUtility
{
public:
	TableUtility();
	int get_color_of_square(tuple<int, int> point);
	int move(tuple<int, int> src, tuple<int, int> dst);
private:
	//Piece* (*_tableP)[]; // points to the 2d array created in session
	//chessBoard *_tableP;
	Piece* table[8][8]; 
private:
	void generate_pawns();
	void generate_rooks();
	void generate_bishops();
	void generate_queens();
	void generate_kings();
	void generate_knight();
	void generate_nulls();
	Piece* GetPoint(tuple <int, int> coords);
	bool check(tuple<int, int> src, tuple<int, int> dst);
	int isPossibleMove(tuple <int, int> src, tuple<int, int> dst);
	int isPossibleMovePawn(tuple <int, int> src, tuple<int, int> dst);
	int isPossibleMoveQueen(tuple <int, int> src, tuple<int, int> dst);
	int isPossibleMoveRook(tuple <int, int> src, tuple<int, int> dst);
	int isPossibleMoveBishop(tuple <int, int> src, tuple<int, int> dst);
	int isPossibleMoveKnight(tuple <int, int> src, tuple<int, int> dst);
	int isPossibleMoveKing(tuple <int, int> src, tuple<int, int> dst);
	int check_amount_of_pieces(bool color) const;
	tuple<int, int> GetKing(bool color);
	bool canKingGoThere(bool color, tuple<int, int> dst);
	std::vector<Piece*> getAllPieces(bool color);
	bool check_mate(bool color);
	bool is_threatening_to_the_point(Piece* piece, tuple<int, int> point);
};

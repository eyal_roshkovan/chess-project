#include "Knight.h"

using std::make_tuple;

Knight::Knight(bool _type, tuple<int, int> _coords) : Piece(_type, _coords, knight)
{
	addPossibleMoves();
}

int Knight::isPossibleMove(const tuple<int, int>& new_coords) const
{
	if (Piece::isPossibleMove(new_coords) == 0)
	{
		return 0;
	}
	tuple<int, int> current = get_coords();
	int old_x = std::get<1>(current);
	int old_y = std::get<0>(current);
	int new_x = std::get<1>(new_coords);
	int new_y = std::get<0>(new_coords);
	
	for (int i = 0; i < 8; i++)
	{
		if (old_x + std::get<0>(_possibleMoves[i]) == new_x && old_y + std::get<1>(_possibleMoves[i]) == new_y)
    {
			return i + 1;
    }
	}
	return 0;
}

int Knight::is_check(const tuple<int, int>& king_location) const
{
	if(isPossibleMove(king_location)>0)
		return isPossibleMove(king_location) + 4;

	return 0;
}

void Knight::addPossibleMoves()
{
	_possibleMoves.push_back(make_tuple(1, 2));
	_possibleMoves.push_back(make_tuple(2, 1));
	_possibleMoves.push_back(make_tuple(2, -1));
	_possibleMoves.push_back(make_tuple(1, -2));
	_possibleMoves.push_back(make_tuple(-1, -2));
	_possibleMoves.push_back(make_tuple(-2, -1));
	_possibleMoves.push_back(make_tuple(-2, 1));
	_possibleMoves.push_back(make_tuple(-1, 2));
}

/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project,
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <string>
#include <iostream>
#include <thread>

using std::cout;
using std::endl;


int main()
{
	srand(time_t(NULL));


	Pipe p;
	bool isConnect = p.connect();

	std::string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return 1;
		}
	}


	char msgToGraphics[1024];
	char code[2];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...

	p.sendMessageToGraphics(msgToGraphics);   // send the board string
	Session s(msgToGraphics);

	// get message from graphics
	std::string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		int code2 = s.updateTable(msgFromGraphics);
		// YOUR CODE
		sprintf_s(msgToGraphics, "%d", code2);
		
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
			msgFromGraphics = p.getMessageFromGraphics();
	}
	s.end_game();
	p.close();

	return 0;
}
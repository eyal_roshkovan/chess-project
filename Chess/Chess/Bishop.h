#pragma once
#include "Piece.h"
class Bishop : public Piece
{
public:
	Bishop(bool _type, tuple <int, int> _coords);
	virtual int isPossibleMove(const tuple<int, int>& new_coords) const override;
	virtual int is_check(const tuple<int, int>& king_location) const override;
};


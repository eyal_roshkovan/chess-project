#pragma once
#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"
class Queen : public Piece
{
public:
	Queen(bool _type, tuple <int, int> _coords);
	virtual int isPossibleMove(const tuple<int, int>& new_coords) const override;
	virtual int is_check(const tuple<int, int>& king_location) const override;
};
